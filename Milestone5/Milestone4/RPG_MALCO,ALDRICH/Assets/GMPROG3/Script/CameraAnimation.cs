﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraAnimation : MonoBehaviour {
    public GameObject Ppos;
    public GameObject Mpos;
    public GameObject Npos;
    public GameObject PanelP;
    public GameObject PanelM;
    public GameObject PanelN;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void MCam()
    {
        this.gameObject.transform.position = Mpos.transform.position;
        PanelP.gameObject.SetActive(false);
        PanelM.gameObject.SetActive(true);
        PanelN.gameObject.SetActive(false);
    }
    public void PCam()
    {
        this.gameObject.transform.position = Ppos.transform.position;
        PanelP.gameObject.SetActive(true);
        PanelM.gameObject.SetActive(false);
        PanelN.gameObject.SetActive(false);
    }
    public void NCam()
    {
        this.gameObject.transform.position = Npos.transform.position;
        PanelP.gameObject.SetActive(false);
        PanelM.gameObject.SetActive(false);
        PanelN.gameObject.SetActive(true);
    }
}
