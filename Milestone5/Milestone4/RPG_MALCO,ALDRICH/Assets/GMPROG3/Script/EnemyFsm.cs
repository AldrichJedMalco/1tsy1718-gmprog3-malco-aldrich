﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyFsm : MonoBehaviour {

    public enum State
    {
        Idle,
        Patrol,
        Chase,
        Attack,
    }
    public State curState;
    public float Timer;
    public float PatrolTimer;
    public bool Location;
    public bool PlayDead = true;
    public Transform NearTarget;
    private Animator Anim;
    private NavMeshAgent Agent;
    private float TimerDestroy;
    KokoDetection KokoDetect;
    Stats Statss;
    EnemySpawner EnemySpawners;
	// Use this for initialization
	void Start () {
        Anim = GetComponent<Animator>();
        Agent = GetComponent<NavMeshAgent>();
        KokoDetect = (KokoDetection)FindObjectOfType(typeof(KokoDetection));
        EnemySpawners = (EnemySpawner)FindObjectOfType(typeof(EnemySpawner));
        Statss = GetComponent<Stats>();
        Timer = 5f;
        PatrolTimer = 7f;
        TimerDestroy = 3f;
	}
	
	// Update is called once per frame
	void Update () {
        if (Statss.CurrentHp <= 0)
        {
            Anim.SetInteger("Anims", 7);
            PlayDead = false;
            TimerDestroy -= Time.deltaTime;
            if (TimerDestroy <= 0)
            {
                TimerDestroy = 0;
                Destroy(this.gameObject);
                EnemySpawners.EnemySpawned--;
                Debug.Log("Namatay Ako");
            }
            return;
        }
            switch (curState)
            {
                case State.Idle: UpdateIdle(); break;
                case State.Patrol: UpdatePatrol(); break;
                case State.Chase: UpdateChase(); break;
                case State.Attack: UpdateAttack(); break;
            }
	}
    void UpdateIdle()
    {
        Anim.SetInteger("Anims", 1);
        Timer -= Time.deltaTime;
        if (Timer < 0)
        {
            Timer = 0;
            Location = true;
            curState = State.Patrol;
        }
        if (NearTarget != null)
            curState = State.Chase;
    }
    void UpdatePatrol()
    {
        Anim.SetInteger("Anims", 2);
        if (Location == true)
        {
            Vector3 randPos = new Vector3(Random.Range(this.transform.position.x - 20, this.transform.position.x + 20), this.transform.position.y, Random.Range(this.transform.position.z - 20, this.transform.position.z + 20));
            this.Agent.SetDestination(randPos);
            Location = false;
        }
        PatrolTimer -= Time.deltaTime;
        if (Vector3.Distance(Agent.destination,Agent.transform.position) < 0.5f || PatrolTimer < 0)
        {
            PatrolTimer = 0;
            Timer = 5;
            curState = State.Idle;
            PatrolTimer = 7;
            Agent.ResetPath();
        }
        if (NearTarget != null)
            curState = State.Attack;
        
    }
    void UpdateChase()
    {
        Anim.SetInteger("Anims", 2);
        if (NearTarget != null)
        {
            Agent.SetDestination(NearTarget.transform.position);
            if (Vector3.Distance(Agent.destination,Agent.transform.position) <= 2.5f)
            {
                curState = State.Attack;
            }
            return;
        }
        if (NearTarget == null)
            curState = State.Patrol;

        Agent.ResetPath();
    }
    void UpdateAttack()
    {
        Anim.SetInteger("Anims", 4);
        if (Vector3.Distance(NearTarget.transform.position, Agent.transform.position) > 1f)
        {
            Debug.Log("Layo Mo Putangian");
            curState = State.Chase;
        }
        if (NearTarget == null)
            curState = State.Patrol;
    }
    void DamageTo()
    {
        if (NearTarget != null)
        {
            Stats Dmg = NearTarget.GetComponent<Stats>();
            Dmg.DecreaseEHealth(Statss.AtkDmg);
        }
    }
}