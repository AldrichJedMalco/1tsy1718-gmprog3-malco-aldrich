﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class HealthBar : MonoBehaviour {

    public Slider Sliders;
    public Stats Stats;
    public GameObject Object;
    private Camera Cams;
	// Use this for initialization
	void Start () {
        Sliders = GetComponent<Slider>();
        Stats = Object.GetComponent<Stats>();
        //Cams = GetComponent<Camera>();
        Cams = Camera.main;
        Sliders.maxValue = Stats.Health;
        Sliders.minValue = 0;
    }
	
	// Update is called once per frame
	void Update () {
        Sliders.value = Stats.CurrentHp;
        this.transform.LookAt(Cams.transform.position);
	}
}
