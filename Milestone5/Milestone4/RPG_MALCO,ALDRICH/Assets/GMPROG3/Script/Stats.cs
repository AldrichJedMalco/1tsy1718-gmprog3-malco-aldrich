﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stats : MonoBehaviour {

    public int Health;
    public int CurrentHp;
    public int AtkDmg;
    public int Vit;
    public int Str;
    private int PrevAtk;
    private int PrevHp;
    public bool VitBoost;
    public bool StrBoost;
	// Use this for initialization
	void Awake () {
    }
	
	// Update is called once per frame
	void Update () {
    }
    public void DecreaseEHealth(int tempDmg)
    {
        CurrentHp -= tempDmg;
    }
    public void UpdateHealth()
    {
        CurrentHp = Health;
    }

    public int BoostVit
    {
        get
        {
            return Health = Health + (Vit * 10);
        }
    }
    public int BoostStr
    {
        get
        {
            return AtkDmg = AtkDmg + (Str * 2);
        }
    }
}
