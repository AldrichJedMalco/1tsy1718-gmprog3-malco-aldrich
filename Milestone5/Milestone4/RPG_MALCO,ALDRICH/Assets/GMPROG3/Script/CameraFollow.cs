﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    public GameObject Player;
    public Vector3 Offset;
    public float Speed;

    private Vector3 _velocity = Vector3.zero;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 lookat = new Vector3(Player.transform.position.x + Offset.x, Player.transform.position.y + Offset.y, Player.transform.position.z + Offset.z);
        transform.position = Vector3.Lerp(this.gameObject.transform.position, lookat, Speed * Time.deltaTime);
	}
}
