﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Character : MonoBehaviour
{
    public enum PlayerFms
    {
        Idle,
        Run,
        Chase,
        Attack,
        Skill,
    }
    // Use this for initialization
    public PlayerFms PlayerState;
    private NavMeshAgent Agent;
    private Animator Anim;
    Stats Statsss;
    public GameObject Cursor;
    public Transform Target;
    void Start()
    {
        Statsss = (Stats)FindObjectOfType(typeof(Stats));
        Agent = GetComponent<NavMeshAgent>();
        Anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        switch (PlayerState)
        {
            case PlayerFms.Idle: UpdateIdle(); break;
            case PlayerFms.Run: UpdateRun(); break;
            case PlayerFms.Chase: UpdateChase(); break;
            case PlayerFms.Attack: UpdateAttack(); break;
        }


        RaycastHit hit;
        if (Input.GetMouseButtonDown(1))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
            {
                Target = null;
                Cursor.transform.position = hit.point;
                Cursor.SetActive(true);
                PlayerState = PlayerFms.Run;
                if (Physics.Raycast(ray, out hit, 200, 1 << 8))
                {
                    PlayerState = PlayerFms.Chase;
                    Target = hit.transform;
                }
                else Agent.SetDestination(hit.point);
            }
        }
    }
    void UpdateIdle()
    {
        Anim.SetInteger("Anims", 3);
    }
    void UpdateRun()
    {
        Anim.SetInteger("Anims", 1);
        if (Vector3.Distance(Agent.destination, Agent.transform.position) < 0.5f)
        {
            Cursor.SetActive(false);
            PlayerState = PlayerFms.Idle;
            Debug.Log("WayPoint Reached");
        }
    }
    void UpdateChase()
    {
        Cursor.SetActive(false);
        Agent.ResetPath();
        Anim.SetInteger("Anims", 1);
        Agent.SetDestination(Target.transform.position);
        if (Vector3.Distance(Agent.destination, Agent.transform.position) <= 2f)
        {
            PlayerState = PlayerFms.Attack;
        }
    }
    void UpdateAttack()
    {
        Anim.SetInteger("Anims", 2);
        EnemyFsm fsm = Target.GetComponent<EnemyFsm>();
        if (Target == null || fsm.PlayDead == false )
        {
            PlayerState = PlayerFms.Idle;
            return;
        }
        if (Vector3.Distance(Target.transform.position, Agent.transform.position) > 3f)
        {
            Debug.Log("Layo Mo Putangian");
            PlayerState = PlayerFms.Chase;
        }
    }
    void DamageTo()
    {
        if (Target != null)
        {
            Stats Dmg = Target.GetComponent<Stats>();
            Dmg.DecreaseEHealth(Statsss.AtkDmg);
        }
    }
    void StartEffect()
    {
    }
    void StopEffect()
    {
    }
}
