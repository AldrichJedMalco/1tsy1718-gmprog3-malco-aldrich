﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KokoDetection : MonoBehaviour {

    public Transform KokoDetect;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    void OnTriggerEnter(Collider temp)
    {
        if (temp.gameObject.tag == "player")
        {
            Debug.Log(temp.gameObject.tag);
            KokoDetect = temp.gameObject.transform;
            GetComponentInParent<EnemyFsm>().NearTarget = KokoDetect;
        }
    }
    void OnTriggerExit(Collider temp)
    {
        if (temp.gameObject.tag == "player")
        {
            KokoDetect = null;
            GetComponentInParent<EnemyFsm>().NearTarget = KokoDetect;
        }
    }
}
