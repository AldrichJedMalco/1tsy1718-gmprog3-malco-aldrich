﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stats : MonoBehaviour {

    public int Health;
    public int CurrentHp;
    public int AtkDmg;
    public int Vit;
    public int Str;
    public int GoldValue;
    public int ManaRegen;
    public int Mana;
    public int CurrentMana;
    public int Spr;
    private int PrevAtk;
    private int PrevHp;
	// Use this for initialization
	void Awake () {
    }
	
	// Update is called once per frame
	void Update () {
    }
    public void ManaRegenFormula()
    {
        ManaRegen = Spr / 10;
    }
    public void DecreaseEHealth(int tempDmg)
    {
        CurrentHp -= tempDmg;
    }
    public void DecreaseMana(int tempMPCost)
    {
        CurrentMana -= tempMPCost;
    }
    public void AddHealth(int tempHp)
    {
        CurrentHp += tempHp;
        if(CurrentHp >= Health)
        {
            CurrentHp = Health;
        }
    }
    public void UpdateHealth()
    {
        CurrentHp = Health;
    }
    public void AddGold(int temp)
    {
        GoldValue += temp;
    }
    public int BoostVit
    {
        get
        {
            return Health = Health + (Vit * 2);
        }
    }
    public int BoostStr
    {
        get
        {
            return AtkDmg = AtkDmg + (Str * 8/10);
        }
    }
    public int BoostSpr
    {
        get
        {
            return Mana = Mana + (Spr * 2);
        }
    }
    public int ReduceSpr
    {
        get
        {
            return Mana = Spr * 2;
        }
    }
    public int ManaRegeneration
    {
        get
        {
            return CurrentMana = CurrentMana + ManaRegen;
        }
    }
    public void StrPotionBoost(int Boost, int Deduct)
    {
        AtkDmg += Boost;
        AtkDmg -= Deduct;
    }
}
