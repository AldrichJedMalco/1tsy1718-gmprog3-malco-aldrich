﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class StatsButton : MonoBehaviour {
    PlayerStats PlayersStatss;

    public Text VitText;
    public Text StrText;
    public Text LevelText;
    public Text SkillAtkText;
    public Text ExpText;
    public Text GoldText;
    public Text StatsPointsLeft;
    public Text PlayerLvl;
    public Text ManaText;
	// Use this for initialization
	void Start () {
        PlayersStatss = (PlayerStats)FindObjectOfType(typeof(PlayerStats));
	}
	
	// Update is called once per frame
	void Update () {
        SetVitText();
        SetStrText();
        SetSkillAtkText();
        SetExpText();
        SetGoldText();
        SetLevelText();
        SetStatsPointsText();
        SetPlayerLvlText();
        SetManaText();
	}
    public void AddVIT()
    {
        if (PlayersStatss.StatPointsLeft <= 0)
            return;

        PlayersStatss.Vit += 1;
        PlayersStatss.CurrentHp = PlayersStatss.BoostVit;
        PlayersStatss.StatPointsLeft--;
    }
    public void AddStr()
    {
        if (PlayersStatss.StatPointsLeft <= 0)
            return;

        PlayersStatss.Str += 1;
        PlayersStatss.AtkDmg = PlayersStatss.BoostStr;
        PlayersStatss.StatPointsLeft--;
    }
    public void SetVitText()
    {
        VitText.text = "Hp: " + PlayersStatss.CurrentHp.ToString() + "/" + PlayersStatss.Health.ToString();
    }
    public void SetStrText()
    {
        StrText.text = "Attack " + PlayersStatss.AtkDmg.ToString();
    }
    public void SetSkillAtkText()
    {
        SkillAtkText.text = "SkillAttack: ";
    }
    public void SetExpText()
    {
        ExpText.text = "Experience: " + PlayersStatss.CurrentExp.ToString();
    }
    public void SetGoldText()
    {
        GoldText.text = "Gold: " + PlayersStatss.GoldValue.ToString();
    }
    public void SetLevelText()
    {
        LevelText.text = "Level: " + PlayersStatss.CurrentLevel.ToString();
    }
    public void SetStatsPointsText()
    {
        StatsPointsLeft.text = "Skill Points: " + PlayersStatss.StatPointsLeft.ToString();
    }
    public void SetPlayerLvlText()
    {
        PlayerLvl.text = PlayersStatss.CurrentLevel.ToString();
    }
    public void SetManaText()
    {
        ManaText.text = "Mana: " + PlayersStatss.CurrentMana.ToString();
    }
}
