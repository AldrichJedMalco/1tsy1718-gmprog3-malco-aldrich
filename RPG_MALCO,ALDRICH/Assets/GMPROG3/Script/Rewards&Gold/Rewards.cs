﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rewards : MonoBehaviour {

    public GameObject Gold;
    public GameObject[] Item;
    public bool DropNow;
    EnemyStats EnemySts;
    int ItemIndex;
    int GoldIndex;
    GameObject SelectedItem;
	// Use this for initialization
	void Start () {
        EnemySts = GetComponent<EnemyStats>();
        DropNow = true;
	}
	
	// Update is called once per frame
	void Update () {
        if (EnemySts.CurrentHp <= 0)
        {
            while (DropNow == true)
            {
                ItemIndex = Random.Range(0, Item.Length);
                GoldIndex = Random.Range(0, 2);
                SelectedItem = Item[ItemIndex];
                Instantiate(SelectedItem, new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z), Quaternion.identity);
                Debug.Log("Item has spawned or It did not spawn");
                if (GoldIndex == 1)
                {
                    Instantiate(Gold, new Vector3(this.transform.position.x, this.transform.position.y + 1, this.transform.position.z), Quaternion.identity);
                }
                else Debug.Log("Hindi ka tumama sa gold coin");
                DropNow = false;
                return;
            }

        }
	}
}
