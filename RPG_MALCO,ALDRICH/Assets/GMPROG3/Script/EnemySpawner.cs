﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {

    public float EnemySpawned = 0;
    public float StartingEnemy = 0;
    public float EnemyMax = 30;
    public float SpawnInterval;
    public GameObject[] Enemy;
    private int ChosenEnemy;
    private GameObject EnemyToSpawn;
    // Use this for initialization
    void Start()
    {
        StartCoroutine(MyMethod());
    }

    // Update is called once per frame
    IEnumerator MyMethod()
    {
        for (int o = 0; o < StartingEnemy; o++)
        {
            ChosenEnemy = Random.Range(0, Enemy.Length);
            EnemyToSpawn = Enemy[ChosenEnemy];
            Instantiate(EnemyToSpawn, new Vector3(Random.Range(-68, 66), 0, Random.Range(-70, 69)), Quaternion.identity);
            EnemySpawned++;
        }
        EnemySpawned = StartingEnemy;
        while (true)
        {
            yield return new WaitForSeconds(SpawnInterval);
            if (EnemySpawned < EnemyMax)
            {
                ChosenEnemy = Random.Range(0, Enemy.Length);
                EnemyToSpawn = Enemy[ChosenEnemy];
                Instantiate(EnemyToSpawn, new Vector3(Random.Range(-68, 66), 0, Random.Range(-70, 69)), Quaternion.identity);
                EnemySpawned++;
            }

        }
    }
}
