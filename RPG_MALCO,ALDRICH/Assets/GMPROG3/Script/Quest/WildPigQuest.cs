﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WildPigQuest : QuestBase {
    PlayerStats PlayerSts;
    // Use this for initialization
    void Start () {
        PlayerSts = (PlayerStats)FindObjectOfType(typeof(PlayerStats));
        QuestFinished = false;
	}
	
	// Update is called once per frame
	void Update () {
        if (EnemyCountToKill >= EnemyKillLeft)
        { EnemyKillMaxed = true; }
        SetQuestText();
	}
    public override void QuestActivated(int temp)
    {
        EnemyKillLeft += temp;
        if (EnemyCountToKill >= EnemyKillLeft)
        {
            PlayerSts.CurrentExp += 20;
            PlayerSts.GoldValue += 30;
            Debug.Log("Quest Activated  | Rewards Given ");
        }
    }
    public override void SetQuestText()
    {
        base.SetQuestText();
    }
}
