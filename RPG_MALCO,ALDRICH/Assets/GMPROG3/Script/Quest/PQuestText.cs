﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PQuestText : MonoBehaviour {
    public List<Text> QuestText = new List<Text>();
    // Use this for initialization
    void Start () {
        foreach (Transform child in transform)
        {
            QuestText.Add(child.GetComponent<Text>());
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
