﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestUI : MonoBehaviour {
    public PlayerQuest Player;

    public GameObject FirstQuest;
    public GameObject SecondQuest;
    public GameObject ThirdQuest;
    public GameObject FourthQuest;
    public GameObject FifthQuest;
	// Use this for initialization
	void Start () {
        Player = (PlayerQuest)FindObjectOfType(typeof(PlayerQuest));
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void FirstQActivated()
    {
        if (Player.PlayerQuestList.Count <= 4)
        {

            if (FirstQuest.GetComponent<GobleQuest>().QuestFinished == false)
            {
                GameObject newHp = Instantiate(FirstQuest) as GameObject;
                Player.AddQuest(newHp.GetComponent<QuestBase>());
                FirstQuest.GetComponent<QuestBase>().QuestFinished = true;
                Debug.Log("Goble Quest Activated");
            }
        }
        else Debug.Log("Too Many Quest Activated");
    }
    public void SecondQActivated()
    {
        if (Player.PlayerQuestList.Count <= 4)
        {
            if (SecondQuest.GetComponent<QuestBase>().QuestFinished == false)
            {
                GameObject newHp = Instantiate(SecondQuest) as GameObject;
                Player.AddQuest(newHp.GetComponent<QuestBase>());
                SecondQuest.GetComponent<QuestBase>().QuestFinished = true;
                Debug.Log("Slime Quest Activated");
            }
        }
        else Debug.Log("Too Many Quest Activated");
    }
    public void ThirdQActivated()
    {
        if (Player.PlayerQuestList.Count <= 4)
        {
            if (ThirdQuest.GetComponent<QuestBase>().QuestFinished == false)
            {
                GameObject newHp = Instantiate(ThirdQuest) as GameObject;
                Player.AddQuest(newHp.GetComponent<QuestBase>());
                ThirdQuest.GetComponent<QuestBase>().QuestFinished = true;
                Debug.Log("WildPig Quest Activated");
            }
        }
        else Debug.Log("Too Many Quest Activated");
    }
    public void FourQActivated()
    { 
    
    }
    public void FifthQActivated()
    { 
    
    }
}
