﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestBase : MonoBehaviour {
    public bool QuestFinished;
    public bool EnemyKillMaxed;
    public int EnemyCountToKill;
    public int EnemyKillLeft;
    public Text QuestText;
    public string QuestPlayerText;
    public string Name;
    public bool Activated;
	// Use this for initialization
	void Start () {
        QuestFinished = false;
        Activated = false;
        EnemyKillMaxed = false;
	}
	
	// Update is called once per frame
	void Update () {
      

        SetQuestText();
	}
    public virtual void QuestActivated(int temp)
    {
        Debug.Log("Quest Accomplished");
    }
    public virtual void QuestCanceled()
    {
        if (EnemyKillLeft == EnemyCountToKill)
        {
            QuestFinished = true;
        }
    }
    public virtual void SetQuestText()
    {
        QuestText.text = "Kill: " + EnemyCountToKill.ToString() + " number of " + Name;
        QuestPlayerText = "Kill: " + EnemyKillLeft.ToString() + "/" + EnemyCountToKill + " no of " + Name + " Left";
    }
}
