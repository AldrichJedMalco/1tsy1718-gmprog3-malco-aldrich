﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerQuestUI : MonoBehaviour {

    public PlayerQuest PlayerQuests;
    public PQuestText PQuestText;
    public List<Button> QuestButtons = new List<Button>();
    public List<Text> QuestText = new List<Text>();
    // Use this for initialization
    void Start()
    {
        PlayerQuests = (PlayerQuest)FindObjectOfType(typeof(PlayerQuest));
        PQuestText = (PQuestText)FindObjectOfType(typeof(PQuestText));
        foreach (Transform child in transform)
        {
            QuestButtons.Add(child.GetComponent<Button>());
        }
    }
    void Update()
    {
    }
    void OnEnable()
    {
        StartCoroutine(UpdateInventory());
    }
    // Update is called once per frame
    void UpdateInventoryList()
    {
        if(PlayerQuests != null)
        {
            for (int i = 0; i < PlayerQuests.PlayerQuestList.Count; i++)
            {
                PQuestText.QuestText[i].GetComponent<Text>().text = PlayerQuests.PlayerQuestList[i].QuestPlayerText;
                QuestButtons[i].GetComponent<Button>().onClick.AddListener(PlayerQuests.PlayerQuestList[i].QuestCanceled);
                if (PlayerQuests.PlayerQuestList[i].QuestFinished == true)
                {
                    PQuestText.QuestText[i].GetComponent<Text>().text = "No Quest Activated :";
                    PlayerQuests.PlayerQuestList.RemoveAt(i);
                    for (int j = i; j < PQuestText.QuestText.Count; j++)
                    {
                        PQuestText.QuestText[j].GetComponent<Text>().text = "No Quest Activated :";
                    }
                    Debug.Log("Quest Removed From the List");
                }
            }
        }
        
    }
    IEnumerator UpdateInventory()
    {
        while (true)
        {
            UpdateInventoryList();
            yield return new WaitForSeconds(0.05f);
        }
    }
}
