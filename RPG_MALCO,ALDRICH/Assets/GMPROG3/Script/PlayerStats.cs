﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : Stats {

    public int Experience;
    public int CurrentExp;
    public int StatPoints;
    public int StatPointsLeft;
    public int CurrentLevel;
    private bool Regen;
	// Use this for initialization
	void Start () {
        Debug.Log("Putanigan");
        ManaRegenFormula();
        CurrentHp = BoostVit;
        AtkDmg = BoostStr;
        CurrentMana = BoostSpr;
        CurrentLevel = 1;
        Regen = true;
        StartCoroutine(StartMRegen());
    }
	
	// Update is called once per frame
	void Update () {
	}
    public void LevelUp(int temp)
    {
        CurrentExp += temp;
        if (CurrentExp >= Experience)
        {
            CurrentLevel++;
            StatPointsLeft += StatPoints;
            CurrentExp = 0;
            IncrementExp();
            Debug.Log("Leveled Up Na Si Lodi");
        }
    }
    void IncrementExp()
    {
        Experience = Experience * 7/8;
    }
    IEnumerator StartMRegen()
    {
        //yield return new WaitWhile(() => CurrentMana < Mana);
        //CurrentMana = ManaRegeneration;
        while (Regen == true)
        {
            yield return new WaitWhile(() => CurrentMana >= Mana);

            yield return new WaitForSeconds(1f);
            CurrentMana = ManaRegeneration;
            
        }
        
    }
}
