﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ExpBar : MonoBehaviour
{

    public Slider Sliders;
    PlayerStats PlayerStatss;
    public GameObject Object;
    private Camera Cams;
    // Use this for initialization
    void Awake()
    {
        Sliders = GetComponent<Slider>();
        PlayerStatss = (PlayerStats)FindObjectOfType(typeof(PlayerStats));
        Cams = Camera.main;
        Sliders.maxValue = PlayerStatss.Experience;
        Sliders.minValue = 0;
    }

    // Update is called once per frame
    void Update()
    {
        Sliders.maxValue = PlayerStatss.Experience;
        Sliders.value = PlayerStatss.CurrentExp;
    }
}
