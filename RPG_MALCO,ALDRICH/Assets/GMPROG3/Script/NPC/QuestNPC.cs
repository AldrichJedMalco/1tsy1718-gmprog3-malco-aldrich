﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestNPC : MonoBehaviour {

    public GameObject QuestUI;
    public GameObject PresText;
    private bool InRange;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        if (InRange == true)
        {
            if (Input.GetKeyDown(KeyCode.W))
            {
                PresText.SetActive(!PresText.activeInHierarchy);
                QuestUI.SetActive(!QuestUI.activeInHierarchy);
            }
        }
		
	}
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "player")
        {
            Debug.Log("Quest Available");
            PresText.SetActive(true);
            InRange = true;
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "player")
        {
            Debug.Log("Quest Shop Out Of Range");
            QuestUI.SetActive(false);
            PresText.SetActive(false);
            InRange = false;
        }
    }
}
