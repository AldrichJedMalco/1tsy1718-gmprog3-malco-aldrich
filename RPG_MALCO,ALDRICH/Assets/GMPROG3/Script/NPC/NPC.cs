﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NPC : MonoBehaviour {

    public GameObject ShopUi;
    public GameObject PresText;
    private bool InRange;
	// Use this for initialization
	void Start () {
        InRange = false;
	}
	
	// Update is called once per frame
	void Update () {
        if (InRange == true)
        {
            if (Input.GetKeyDown(KeyCode.U))
            {
                PresText.SetActive(!PresText.activeInHierarchy);
                ShopUi.SetActive(!ShopUi.activeInHierarchy);
            }
        }
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "player")
        {
            Debug.Log("Shop Available");
            PresText.SetActive(true);
            InRange = true;
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "player")
        {
            Debug.Log("Shop Out Of Range");
            ShopUi.SetActive(false);
            PresText.SetActive(false);
            InRange = false;
        }
    }
}
