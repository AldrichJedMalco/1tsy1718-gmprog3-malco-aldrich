﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopButtons : MonoBehaviour {
    public GameObject HpSlot;
    public GameObject ManaSlot;
    public GameObject StrSlot;

    Inventory Invent;
    PlayerStats PlayerSts;
	// Use this for initialization
	void Start () {
        Invent = (Inventory)FindObjectOfType(typeof(Inventory));
        PlayerSts = (PlayerStats)FindObjectOfType(typeof(PlayerStats));
        
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void BuyHp()
    {
        if (Invent.InventoryList.Count <= 24)
        {
            if (PlayerSts.GoldValue >= 50)
            {
                GameObject newHp = Instantiate(HpSlot) as GameObject;
                newHp.GetComponent<Item>().Used = false;
                Invent.PickUpItems(newHp.GetComponent<Item>());
                PlayerSts.GoldValue -= 50;
            }
            else Debug.Log("Not Enought Money");
        }
        else Debug.Log("Not enough Space");
        //else Debug.Log("Not Enought Money");
    }
    public void BuyMp()
    {
        if (Invent.InventoryList.Count <= 25)
        {
                if (PlayerSts.GoldValue >= 50)
            {
                GameObject newMana = Instantiate(ManaSlot) as GameObject;
            newMana.GetComponent<Item>().Used = false;
            Invent.PickUpItems(newMana.GetComponent<Item>());
                PlayerSts.GoldValue -= 50;
            }
            else Debug.Log("Not Enought Money");
            }
        else Debug.Log("Not enough Space");
    }
    public void BuyVitality()
    {
        if (Invent.InventoryList.Count <= 25)
        {
            if (PlayerSts.GoldValue >= 50)
        {
            GameObject newStr = Instantiate(StrSlot) as GameObject;
        newStr.GetComponent<Item>().Used = false;
        Invent.PickUpItems(newStr.GetComponent<Item>());
            PlayerSts.GoldValue -= 50;
        }
        else Debug.Log("Not Enought Money");
        }
        else Debug.Log("Not enough Space");
    }

}
