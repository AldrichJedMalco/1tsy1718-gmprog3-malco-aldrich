﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SkillsSelect : MonoBehaviour {

    public BaseSkill CurrentSkill;
    public Sprite SkillSprite;
    public GameObject SelectedSkill;
    public GameObject UiSkill;
    public bool UseSkill;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.E))
        {
            UiSkill.SetActive(!UiSkill.activeInHierarchy);
        }
        SkillSprite = CurrentSkill.SkillSprite;
        SelectedSkill = CurrentSkill.Skill;
	}
}
