﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blessed : BaseSkill
{
    public int Duration;
    PlayerStats PlayerSts;
    // Use this for initialization
    void Start()
    {
        PlayerSts = (PlayerStats)FindObjectOfType(typeof(PlayerStats));
    }

    // Update is called once per frame
    void Update()
    {
        CurrentCoolDown -= Time.deltaTime;
        if (CurrentCoolDown < 0)
        { CurrentCoolDown = 0; }
    }
    public override void UseSkills()
    {
        if (CurrentCoolDown <= 0)
        {
            StartCoroutine(BlessingSkill());
            CurrentCoolDown = CoolDown;
        }
    }
    IEnumerator BlessingSkill()
    {
        PlayerSts.Spr = PlayerSts.Spr + MATK;
        PlayerSts.CurrentMana = PlayerSts.BoostSpr;
        PlayerSts.ManaRegenFormula();
        yield return new WaitForSeconds(Duration);
        PlayerSts.Spr = PlayerSts.Spr - MATK;
        PlayerSts.CurrentMana = PlayerSts.ReduceSpr;
        PlayerSts.ManaRegenFormula();
    }
}
