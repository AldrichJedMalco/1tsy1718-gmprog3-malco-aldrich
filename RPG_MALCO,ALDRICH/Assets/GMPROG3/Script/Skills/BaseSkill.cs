﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseSkill : MonoBehaviour {

    public float CoolDown;
    public float CurrentCoolDown;
    public int MpCost;
    public int MATK;
    public bool Use;
    //private Animator Anim;
    public Sprite SkillSprite;
    public GameObject Skill;
    public SkillsSelect SkillSel;
    public BaseSkill BSkill;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Use = SkillSel.UseSkill;
	}
    public virtual void UseSkills()
    {
        Debug.Log("Skill Used");
    }
    public void SelectSkill()
    {
        SkillSel.CurrentSkill = BSkill;
    }
}
