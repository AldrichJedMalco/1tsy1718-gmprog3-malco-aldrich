﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class SkillButton : MonoBehaviour {
    SkillsSelect SkillSel;
    public Sprite ButtonSprite;
    public Text TextCd;

    public GameObject Button1;
    public GameObject Button2;
    public GameObject Button3;
    PlayerStats PlayerSts;
	// Use this for initialization
	void Start () {
        SkillSel = (SkillsSelect)FindObjectOfType(typeof(SkillsSelect));
        PlayerSts = (PlayerStats)FindObjectOfType(typeof(PlayerStats));
        Button2.SetActive(false);
        Button3.SetActive(false);

    }
	
	// Update is called once per frame
	void Update () {
        this.GetComponent<Image>().sprite = SkillSel.SkillSprite;
        SetText();
        if(PlayerSts.CurrentLevel >= 2)
        {
            Button2.SetActive(true);
        }
        if (PlayerSts.CurrentLevel >= 3)
        {
            Button3.SetActive(true);
        }


    }
    void SkillActivate()
    {
        SkillSel.UseSkill = true;
    }
    public void ActivateSkill()
    {
        SkillSel.CurrentSkill.GetComponent<BaseSkill>().UseSkills();
        
    }
    public void SetText()
    {
        TextCd.text = "Cd: " + SkillSel.CurrentSkill.CurrentCoolDown.ToString();//GetComponent<BaseSkill>().CurrentCoolDown.ToString();
    }

}
