﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heal : BaseSkill {
    PlayerStats PlayerSts;
	// Use this for initialization
	void Start () {
        PlayerSts = (PlayerStats)FindObjectOfType(typeof(PlayerStats));
    }
	
	// Update is called once per frame
	void Update () {

            CurrentCoolDown -= Time.deltaTime;
            if (CurrentCoolDown < 0)
            { CurrentCoolDown = 0; }
        
       
    }
    public override void UseSkills()
    {
        if (CurrentCoolDown <= 0)
        {
            PlayerSts.AddHealth(MATK * 2);
            CurrentCoolDown = CoolDown;
        }
    }
}
