﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundCrush : BaseSkill {
    public Animator Anim;
    Character Charac;
    PlayerStats PlayerSts;
	// Use this for initialization
	void Start () {
        Charac = (Character)FindObjectOfType(typeof(Character));
        PlayerSts = (PlayerStats)FindObjectOfType(typeof(PlayerStats));
    }
	
	// Update is called once per frame
	void Update () {
        CurrentCoolDown -= Time.deltaTime;
        if (CurrentCoolDown < 0)
        { CurrentCoolDown = 0; }
    }
    public override void UseSkills()
    {
        if (PlayerSts.CurrentMana >= 10)
        {
            if (CurrentCoolDown <= 0)
            {
                Charac.PlayerState = Character.PlayerFms.Skill;
                CurrentCoolDown = CoolDown;
                PlayerSts.CurrentMana -= 10;
            }
        }
    }
}
