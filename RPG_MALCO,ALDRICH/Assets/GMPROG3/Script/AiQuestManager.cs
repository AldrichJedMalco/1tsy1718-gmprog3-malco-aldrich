﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AiQuestManager : MonoBehaviour {
    public bool GobleCheck;
    public bool SlimeCheck;
    public bool WildCheck;
    public PlayerQuest PlayerQ;
    public GobleQuest GobleQ;
    public SlimeQuest SlimeQ;
    public WildPigQuest WildQ;
    public GameObject Sample;
	// Use this for initialization
	void Start () {
        GobleCheck = false;
        WildCheck = false;
        SlimeCheck = false;
        PlayerQ = (PlayerQuest)FindObjectOfType(typeof(PlayerQuest));
        GobleQ = (GobleQuest)FindObjectOfType(typeof(GobleQuest));
        SlimeQ = (SlimeQuest)FindObjectOfType(typeof(SlimeQuest));
        WildQ = (WildPigQuest)FindObjectOfType(typeof(WildPigQuest));
        //GobleQ = GetComponent<GobleQuest>();
    }

    // Update is called once per frame
    void Update () {
	}
    public void SendQuestMessage()
    {
        for (int i = 0; i < PlayerQ.PlayerQuestList.Count; i++)
        {
            if (PlayerQ.PlayerQuestList[i] == PlayerQ.PlayerQuestList[i].GetComponent<GobleQuest>() && GobleCheck == true)
            {
                Debug.Log("Found Goble Quest");
                if (GobleCheck == true)
                {
                    PlayerQ.PlayerQuestList[i].GetComponent<GobleQuest>().SendMessage("QuestActivated", 1);
                    GobleCheck = false;
                }
            }

            else if (PlayerQ.PlayerQuestList[i] == PlayerQ.PlayerQuestList[i].GetComponent<SlimeQuest>() && SlimeCheck == true)
            {
                Debug.Log("Found Slime Quest");
                if (SlimeCheck == true)
                {
                    PlayerQ.PlayerQuestList[i].GetComponent<SlimeQuest>().SendMessage("QuestActivated", 1);
                    SlimeCheck = false;
                }
            }
            else if (PlayerQ.PlayerQuestList[i] == PlayerQ.PlayerQuestList[i].GetComponent<WildPigQuest>() && WildCheck == true)
            {
                if (WildCheck == true)
                {
                    Debug.Log("Found WildPig Quest");
                    PlayerQ.PlayerQuestList[i].GetComponent<WildPigQuest>().SendMessage("QuestActivated", 1);
                    WildCheck = false;
                }
            }
        }
    }
}
