﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoldItem : Item {
    PlayerStats PlayerSts;
    EnemyStats EnemySts;
    public int GoldValue;
	// Use this for initialization
	void Start () {
        PlayerSts = (PlayerStats)FindObjectOfType(typeof(PlayerStats));
        EnemySts = (EnemyStats)FindObjectOfType(typeof(EnemyStats));
        //EnemySts = GetComponent<EnemyStats>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public override void Use()
    {
        PlayerSts.AddGold(GoldValue);
    }
}
