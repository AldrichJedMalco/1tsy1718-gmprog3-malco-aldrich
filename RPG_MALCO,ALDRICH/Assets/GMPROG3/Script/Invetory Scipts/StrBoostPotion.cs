﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StrBoostPotion : Item {
    PlayerStats PlayerSts;
    private int Temp;
	// Use this for initialization
	void Start () {
        PlayerSts = (PlayerStats)FindObjectOfType(typeof(PlayerStats));
        Used = false;
	}
	
	// Update is called once per frame
	void Update () {
	}
    public override void Use()
    {
        Used = true;
        StartCoroutine(BuffingTimer());
        Debug.Log("Buffing Vit Start");
        if(Used == false)
        {
            StopCoroutine(BuffingTimer());
        }
    }
    IEnumerator BuffingTimer()
    {
        Temp = PlayerSts.CurrentHp;
        PlayerSts.StrPotionBoost(Value, 0);
        yield return new WaitForSeconds(5f);
        PlayerSts.StrPotionBoost(0,Value);
    }
}
