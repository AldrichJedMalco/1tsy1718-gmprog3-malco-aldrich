﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryButtons : MonoBehaviour {

    public GameObject Ui;
    public GameObject UiQuest;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.I))
        {
            Ui.SetActive(!Ui.activeInHierarchy);
        }
        if (Input.GetKeyDown(KeyCode.Q))
        {
            UiQuest.SetActive(!UiQuest.activeInHierarchy);
        }
	}
}
