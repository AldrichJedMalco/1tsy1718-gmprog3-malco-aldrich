﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpItems : MonoBehaviour {
    Inventory Invent;
	// Use this for initialization
	void Start () {
        Invent = (Inventory)FindObjectOfType(typeof(Inventory));
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<GoldItem>())
        {
            other.GetComponent<GoldItem>().Use();
            other.gameObject.SetActive(false);
        }
        if (other.GetComponent<Item>() && !other.GetComponent<GoldItem>())
        {
            if (Invent.InventoryList.Count <= 24)
            {
                this.GetComponentInChildren<Inventory>().PickUpItems(other.GetComponent<Item>());
                other.gameObject.SetActive(false);
                Debug.Log("Item Added to inventory");
            }
            else Debug.Log("Not enough space in inventory");
        }
    }
}
