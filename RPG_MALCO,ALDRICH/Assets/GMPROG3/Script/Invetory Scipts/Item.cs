﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Item : MonoBehaviour {
    public string Name;
    public int Value;
    public int Price;
    public int Timer;
    public Sprite sprite;
    public bool Used;
	// Use this for initialization
	void Start () {
        Used = false;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public virtual void Use()
    {
        Debug.Log("Item used");
    }
}
