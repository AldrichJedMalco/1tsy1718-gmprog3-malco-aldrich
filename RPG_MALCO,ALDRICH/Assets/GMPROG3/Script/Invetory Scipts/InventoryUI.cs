﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryUI : MonoBehaviour {
    
    public Inventory Player;
    public Sprite defaultsprite;
    public List<Button> InventoryButtons = new List<Button>();
	// Use this for initialization
	void Start () {
        Player = (Inventory)FindObjectOfType(typeof(Inventory));
        foreach (Transform child in transform)
        {
            InventoryButtons.Add(child.GetComponent<Button>());
        }
        StartCoroutine(UpdateInventory());
	}
    void OnEnable()
    {
        UpdateInventory();
    }
	// Update is called once per frame
	void UpdateInventoryList () {
        if (Player.InventoryList != null)
        {
            for (int i = 0; i < Player.InventoryList.Count ; i++)
            {
                InventoryButtons[i].GetComponent<Button>().onClick.AddListener(Player.InventoryList[i].Use);
                InventoryButtons[i].GetComponent<Image>().sprite = Player.InventoryList[i].sprite;
                if (Player.InventoryList[i].Used == true)
                {
                    InventoryButtons[i].GetComponent<Image>().sprite = defaultsprite;
                    Player.InventoryList.RemoveAt(i);
                    Debug.Log("Removed From the List");
                    for (int j = i; j < InventoryButtons.Count ; j++)
                    {
                        InventoryButtons[j].GetComponent<Image>().sprite = defaultsprite;
                    }
                }

            }
        }
        
	}
    IEnumerator UpdateInventory()
    {
        while(true)
        {
            UpdateInventoryList();
            yield return new WaitForSeconds(0.05f);
        }
    }
}
