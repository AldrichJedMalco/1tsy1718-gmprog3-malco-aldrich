﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManaPotion : Item {
    PlayerStats PlayerSts;
    // Use this for initialization
    void Start () {
        PlayerSts = (PlayerStats)FindObjectOfType(typeof(PlayerStats));
        Used = false;
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    public override void Use()
    {
        Used = true;
        Debug.Log("I got Mana Item");
    }

}
