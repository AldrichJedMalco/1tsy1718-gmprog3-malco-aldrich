﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Character : MonoBehaviour
{
    public enum PlayerFms
    {
        Idle,
        Run,
        Chase,
        Attack,
        Skill,
        Dead,
    }
    // Use this for initialization
    public PlayerFms PlayerState;
    private NavMeshAgent Agent;
    private Animator Anim;
    PlayerStats PStatss;
    public GameObject Cursor;
    public Transform Target;
    public GameObject StatsUi;
    public LayerMask EnemyMask;
    void Start()
    {
        PStatss = (PlayerStats)FindObjectOfType(typeof(PlayerStats));
        Agent = GetComponent<NavMeshAgent>();
        Anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        switch (PlayerState)
        {
            case PlayerFms.Idle: UpdateIdle(); break;
            case PlayerFms.Run: UpdateRun(); break;
            case PlayerFms.Chase: UpdateChase(); break;
            case PlayerFms.Attack: UpdateAttack(); break;
            case PlayerFms.Skill: UpdateSkill(); break;
            case PlayerFms.Dead: PlayDead(); break;
        }

        if (Input.GetKeyDown(KeyCode.S))
        {
            StatsUi.SetActive(!StatsUi.activeInHierarchy);
        }
        RaycastHit hit;
        if (PStatss.CurrentHp <= 0)
        {
            PlayerState = PlayerFms.Dead;
            return;
        }
        if (Input.GetMouseButtonDown(1))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
            {
                Target = null;
                Cursor.transform.position = hit.point;
                Cursor.SetActive(true);
                PlayerState = PlayerFms.Run;
                if (Physics.Raycast(ray, out hit, 200, 1 << 8))
                {
                    PlayerState = PlayerFms.Chase;
                    Target = hit.transform;
                }
                else Agent.SetDestination(hit.point);
            }
        }
    }
    void UpdateIdle()
    {
        Anim.SetInteger("Anims", 3);
    }
    void UpdateRun()
    {
        if (PStatss.CurrentHp <= 0)
        {
            PlayerState = PlayerFms.Dead;
        }
        if (Target != null)
        {
            EnemyFsm fsm = Target.GetComponent<EnemyFsm>();
            {
                if (Target == null || fsm.PlayDead == false)
                {
                    PlayerState = PlayerFms.Idle;
                    return;
                }
            }
        }
        Anim.SetInteger("Anims", 1);
        if (Vector3.Distance(Agent.destination, Agent.transform.position) < 0.5f)
        {
            Cursor.SetActive(false);
            PlayerState = PlayerFms.Idle;
            Debug.Log("WayPoint Reached");
        }
    }
    void UpdateChase()
    {
        if (PStatss.CurrentHp <= 0)
        {
            PlayerState = PlayerFms.Dead;
        }
        if (Target != null)
        {
            EnemyFsm fsm = Target.GetComponent<EnemyFsm>();
            {
                if (Target == null || fsm.PlayDead == false)
                {
                    PlayerState = PlayerFms.Idle;
                    return;
                }
            }
        }
        Cursor.SetActive(false);
        Agent.ResetPath();
        Anim.SetInteger("Anims", 1);
        Agent.SetDestination(Target.transform.position);
        if (Vector3.Distance(Agent.destination, Agent.transform.position) <= 2f)
        {
            PlayerState = PlayerFms.Attack;
        }
    }
    void UpdateAttack()
    {
        if (PStatss.CurrentHp <= 0)
        {
            PlayerState = PlayerFms.Dead;
        }
        Anim.SetInteger("Anims", 2);
        if (Target != null)
        {
            EnemyFsm fsm = Target.GetComponent<EnemyFsm>();

            if (Target == null || fsm.PlayDead == false)
            {
                PlayerState = PlayerFms.Idle;
                return;
            }
        }
        if (Vector3.Distance(Target.transform.position, Agent.transform.position) > 3f)
        {
            Debug.Log("Layo Mo Putangian");
            PlayerState = PlayerFms.Chase;
        }
    }
    void UpdateSkill()
    {
        Anim.SetInteger("Anims", 10);
        //PlayerState = PlayerFms.Idle;
    }
    void DamageTo()
    {
        if (Target != null)
        {
            EnemyStats Dmg = Target.GetComponent<EnemyStats>();
            Dmg.DecreaseEHealth(PStatss.AtkDmg);
        }
    }
    void StartEffect()
    {
    }
    void StopEffect()
    {
    }
    void AreaAttack()
    {
        //EnemyStats Dmg = GetComponent<EnemyStats>();
        //Dmg.DecreaseEHealth(PStatss.AtkDmg);
        Collider[] hitColliders = Physics.OverlapSphere(this.transform.position, 10, EnemyMask.value);

        for(int i = 0; i < hitColliders.Length; i++)
        {
            hitColliders[i].GetComponent<EnemyStats>().DecreaseEHealth(PStatss.AtkDmg);
        }
    }
    void PlayDead()
    {
        Anim.SetInteger("Anims", 11);
    }
}
