﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class HealthBar : MonoBehaviour {

    public Slider Sliders;
    public Stats Stats;
    public GameObject Object;
    private Camera Cams;
	// Use this for initialization
	void Awake () {
        Sliders = GetComponent<Slider>();
        Stats = Object.GetComponent<Stats>();
        Cams = Camera.main;
        Sliders.maxValue = Stats.Health;
        Sliders.minValue = 0;
    }
	
	// Update is called once per frame
	void Update () {
        Sliders.maxValue = Stats.Health;
        Sliders.value = Stats.CurrentHp;
        if (this.gameObject.tag == "KokoUi")
        {
            this.transform.LookAt(null);
        }
        else
        this.transform.LookAt(Cams.transform.position);
	}
}
