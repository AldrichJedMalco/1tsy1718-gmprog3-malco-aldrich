﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationController : MonoBehaviour {

    private Animator Anim;
	// Use this for initialization
	void Start () {
		Anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
     
    public void OnClick(int Index)
    {
        Anim.SetInteger("Anims", Index);
    }

}
