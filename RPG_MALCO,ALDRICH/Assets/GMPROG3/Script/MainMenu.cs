﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class MainMenu : MonoBehaviour {
    
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void Animation()
    {
        SceneManager.LoadScene("Animation");
    }
    public void Play()
    {
        SceneManager.LoadScene("Finale");
    }
    public void Exit()
    {
        Application.Quit();
    }

    public void Back2Main()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
